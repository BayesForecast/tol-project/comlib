#Require ComLib;

//////////////////////////////////////////////////////////////////////////////
// El est�ndar de JSON no permite representar fechas (o instantes) de manera
// natural, de modo que se ha de hacer a trav�s de n�meros o cadenas de texto.
// Hay dos est�ndares usados con frecuencia para representar fechas, mediante
// cada una de estas posibilidades.
// 
//  * ISO-8601: Utiliza codificaciones en forma de texto.
//    V�ase: https://es.wikipedia.org/wiki/ISO_8601
//    Concretamente usaremos la fecha calendario (con mes y d�a):
//      + "yyyy-mm-dd" para fechas
//      + "yyyy-mm-ddThh:ii:ssZ" para instantes hasta el segundo
//      + "yyyy-mm-ddThh:ii:ss.ssZ" para instantes hasta el centisegundo
//    Para indicar zona horaria distinta a UTC, se sustituye la Z por 
//    la franja horaria en formato (+|-)hh:ii.
//    Por ejemplo: "2017-02-10T09:30:00+01:00" es "2017-02-10T08:30:00Z"
// 
// * Tiempo UNIX o POSIX: Utiliza el tiempo transcurrido desde una fecha 
//   de referencia. Concretamente la medianoche UTC del 1 de enero de 1970.
//   V�ase: https://es.wikipedia.org/wiki/Tiempo_Unix
//   Hay dos posibilidades para representar este tiempo:
//     + Segundos transcurridos. Por ejemplo: 1486719000
//     + Milisegundos transcurridos: Por ejemplo: 1486719000000
// 
// Estas representaciones, sin embargo, no solucionan la ausencia del tipo
// de datos en JSON, y no son reversibles de manera directa, sin contexto.
// No podemos saber por el dato si se trata de una fecha o de un n�mero o 
// un texto con aspecto de fecha.
// 
//----------------------------------------------------------------------------
// JSONx: "date"
// Para evitar la ambig�edad en la representaci�n de las fechas usaremos 
// la clase "date".

Text ToJSON(y2017m02d10);
//> {"$date":1486684800000}

// Esta representaci�n de las fechas mediante la clase "date" y el tiempo 
// UNIX es reconocida naturalmente por el paquete de R jsonlite:
/*  
library(jsonlite);
fromJSON('{"$date":1486684800000}');
*/

//----------------------------------------------------------------------------
// Format ISO-8601
// Si se desea utilizar el formato ISO-8601 en lugar del tiempo UNIX podemos 
// usar la opci�n "format_Date" con el valor "ISO8601".
// �sta utilizar� las tres variantes mencionadas seg�n sean fechas, instantes   
// hasta el segundo o instantes hasta el centisegundo (m�xima precisi�n en TOL).

Text ToJSON2(y2017m02d10, [[ Text format_Date = "ISO8601" ]]);
//> {"$date":"2017-02-10"}

Text ToJSON2(y2017m02d10h09i30s05, [[ Text format_Date = "ISO8601" ]]);
//> {"$date":"2017-02-10T09:30:05Z"}

Text ToJSON2(y2017m02d10h09i30s04.67, [[ Text format_Date = "ISO8601" ]]);
//> {"$date":"2017-02-10T09:30:04.67Z"}

//----------------------------------------------------------------------------
// JSON: <array>
// JSONx: "date" (como array)
// Un conjunto de fechas puede almacenarse como un vector en la clase "date",
// de manera similar a como se hac�a con los n�meros (clase "number").
// V�ase el archivo "json_numbers.tol"

Text ToJSON(SetOfDate(y2001, y2002, y2003));
//> {"$date":[978307200000, 1009843200000, 1041379200000]}

// Las fechas (o conjunto de ellas) pueden obtenerse sin la clase a trav�s
// de la opcion "use_date". En este caso el formato por defecto es "ISO8601".
// Si se quiere usar el formato POSIX lo indicaremos con "format_Date".

Text ToJSON2(SetOfDate(y2001, y2002, y2003), 
  [[ Real use_date = False, Text format_Date = "POSIX" ]]);
//> [978307200000, 1009843200000, 1041379200000]

//////////////////////////////////////////////////////////////////////////////
