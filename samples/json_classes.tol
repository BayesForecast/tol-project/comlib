
#Require ComLib;

//////////////////////////////////////////////////////////////////////////////
// JSONx: Extended JSON
// El est�ndar de JSON est� restringido a un conjunto muy elemental de tipos
// de modo que el resto de tipos de objeto han de ser serializados para
// representarlos en JSON.
// 
// Los tipos que pueden representarse en un JSON son:
//   <string>: Cadenas de texto.
//   <number>: N�meros finitos. No admite el valor desconocido ni los infinitos.
//   <boolean>: Valores l�gicos: 'true' y 'false'.
//   <array>: Conjunto de elementos de cualquier tipo.
//   <object>: Conjunto de elementos de cualquier tipo con nombre.
//   'null': El valor nulo.
// 
// Para representar otro tipo de datos, usaremos una serializaci�n que pueda
// representarse b�sicamente como conjuntos de textos y n�meros.
// 
//----------------------------------------------------------------------------
// EJEMPLO:
// Consideremos el tipo de datos de los n�meros complejos.
// Un n�mero complejo puede representase sin ambig�edad por un par de n�meros
// reales, uno con su parte real y otro con su parte imaginaria.
//
// Para representar el n�mero 1+2i podemos usar un array:
// [1, 2]
// O siendo un poco m�s expl�citos, mediante un objeto en el que indicaremos
// cual es la parte real y la parte imaginara:
// {"re":1, "im":2}
// 
// Sin embargo, esta serializaci�n puede revertirse y construir con ella 
// n�meros complejos si la funci�n que ha de hacerlo ya sabe que se tratan 
// de n�meros complejos.
// De lo contrario, podr�a simplemente interpretar que son pares de n�meros.
//
Complex c = 1 + 2*i;

Set c_ser1 = [[CReal(c), CImag(c)]];
Text ToJSON(c_ser1);
//> [1, 2]

Set c_ser2 = {[[Real re = CReal(c), Real im = CImag(c)]]};
Text ToJSON(c_ser2);
//> {
//>  "re":1,
//>  "im":2
//> }

//----------------------------------------------------------------------------
// Clases en JSON
// Para facilitar la interpretaci�n del JSON, se puede incorporar en el  
// objeto un miembro adicional con el nombre de la clase.
// Para ello usaremos un miembro con el nombre "$class" y cuyo contenido
// sea el nombre del tipo de datos.
// As�, el n�mero complejo anterior podr�a representarse como:
// {"$class":"complex", "re":1, "im":2}

Set c_ser3 = {[[
  Text _.._class = "complex", // '_.._' representa al $ en el nombre TOL
  Real re = CReal(c), 
  Real im = CImag(c)
]]};
Text ToJSON(c_ser3);
//> {
//>   "$class":"complex",
//>   "re":1,
//>   "im":2
//> }

//----------------------------------------------------------------------------
// Clases sencillas en JSON
// Si la serializaci�n del tipo de datos es sencilla y puede representarse
// mediante un �nico miembro, podemos simplificar su representaci�n 
// y hacerla m�s compacta asignando al nombre de este miembro el nombre 
// de la clase.
// As�, usando la representaci�n del n�mero complejo como un array,
// podemos escribir el n�mero complejo anterior como:
// {"$complex":[1, 2]}

Set c_ser4 = {[[ Set _.._complex = [[CReal(c), CImag(c)]] ]]};
Text ToJSON(c_ser4);
// {"$complex":[1, 2]}

//////////////////////////////////////////////////////////////////////////////
// Tipos de datos y clases utlizadas en este paquete:

// Tipos y clases elementales:
//----------------------------
// <string>      Text
// <number>      Real* (real finito)
// "number"      Real
// <array>       Set* (conjunto sin nombres)
// <object>      Set* (conjunto con nombres)
// "date"        Date
// "complex"     Complex
// "matrix"      Matrix
// "timeseries"  Serie* (serie limitada sin fechado asociado)

// Est�ndares de TOL utilizados en R, v�a tolBasis:
//-------------------------------------------------
// "Dating"      TimeSet* (fechado est�ndar o limitados)
// "Serie"       Serie* (serie limitada con fechado est�ndar)
// "Polyn"       Polyn
// "Ratio"       Ratio

// Tipos de datos t�picos de TOL
//------------------------------
// "TOL:NameBlock"     NameBlock
// "TOL:VMatrix"       VMatrix
// "TOL:<StructName>"  (Struct)
// "TOL:<ClassName>"   (Class)
// (sin implementar a�n)
// ? "TOL:Code"        Code
// ? "TOL:PolMatrix"   PolMatrix
// ? "TOL:TimeSet"     TimeSet* (fechados ilimitados)
// ? "TOL:Serie"       Serie* (series ilimitadas)

//////////////////////////////////////////////////////////////////////////////
